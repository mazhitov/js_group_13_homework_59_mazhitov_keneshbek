import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { QuestionsService } from './shared/Questions.service';
import { QuestionsComponent } from './questions/questions.component';
import { AnswerComponent } from './answer/answer.component';
import { FormsModule } from '@angular/forms';
import { CorrectOrIncorrectAnswerDirective } from './directives/correctOrIncorrectAnswer.directive';
import { ShowHelpTextDirective } from './directives/showHelpText.directive';

@NgModule({
  declarations: [
    AppComponent,
    QuestionsComponent,
    AnswerComponent,
    CorrectOrIncorrectAnswerDirective,
    ShowHelpTextDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [QuestionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
