import { Component, OnInit } from '@angular/core';
import { QuestionsService } from './shared/Questions.service';
import { Question } from './shared/Question.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  questions: Question[] = [];
  result = 0;

  constructor(private questionsService: QuestionsService) {}

  ngOnInit(){
    this.questions = this.questionsService.getQuestions();
    this.result = this.questionsService.getResult();
    this.questionsService.resultChange.subscribe(result => {
      return this.result = result;
    });
  }
}
