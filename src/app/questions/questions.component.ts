import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../shared/Questions.service';
import { Question } from '../shared/Question.model';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {
  questions!:Question[];

  constructor(private questionsService: QuestionsService) {}

  ngOnInit(): void {
    this.questions = this.questionsService.getQuestions();
  }

}
